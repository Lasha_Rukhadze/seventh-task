package com.example.seventhtask

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.seventhtask.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    lateinit var binding: ActivityMainBinding
    private val organs = mutableListOf<ThingsModel>()
    lateinit var adapter: RecyclerViewAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initializer()
        setter()
    }

    private fun initializer(){
        adapter = RecyclerViewAdapter(organs)
        binding.recycler.layoutManager = LinearLayoutManager(this)
        binding.recycler.adapter = adapter
    }

    private fun setter(){
        organs.add(ThingsModel(R.drawable.ic_heart, "Heart", "Weight: 325 Grams"))
        organs.add(ThingsModel(R.drawable.ic_liver, "Liver", "Weight: 1400 Grams"))
        organs.add(ThingsModel(R.drawable.ic_lungs, "Lungs", "Weight: 1100 Grams"))
        organs.add(ThingsModel(R.drawable.ic_gallbladder, "Gallbladder", "Weight: 300 Grams"))
        organs.add(ThingsModel(R.drawable.ic_stomach, "Stomach", "Weight: 910 Grams"))
        organs.add(ThingsModel(R.drawable.ic_pancreas, "Pancreas", "Weight: 100 Grams"))
        organs.add(ThingsModel(R.drawable.ic_tongue, "Tongue", "Weight: 65 Grams"))
        organs.add(ThingsModel(R.drawable.ic_spleen, "Spleen", "Weight: 170 Grams"))
        organs.add(ThingsModel(R.drawable.ic_large_intestine, "Intestines", "Weight: 2000 Grams"))
        organs.add(ThingsModel(R.drawable.ic_thyroid, "Thyroid", "Weight: 35 Grams"))
        organs.add(ThingsModel(R.drawable.ic_muscle, "Muscles", "Weight: 35 Kg"))

        adapter.notifyDataSetChanged()
    }

}