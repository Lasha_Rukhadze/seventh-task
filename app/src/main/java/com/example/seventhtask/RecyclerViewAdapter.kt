package com.example.seventhtask

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.seventhtask.databinding.ThingsLayoutBinding

class RecyclerViewAdapter(private val organs : MutableList<ThingsModel>) :
    RecyclerView.Adapter<RecyclerViewAdapter.ItemViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val thingsView = ThingsLayoutBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        val holder = ItemViewHolder(thingsView)
        return holder
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
       /* val model = organs[position]
        holder.binding.image.setImageResource(model.image)
        holder.binding.name.text = model.name
        holder.binding.weight.text = model.weight*/
        holder.bind()
    }

    override fun getItemCount(): Int {
        return organs.size
    }

    inner class ItemViewHolder(val binding : ThingsLayoutBinding) : RecyclerView.ViewHolder(binding.root) {

        private lateinit var model: ThingsModel
        fun bind() {
            model = organs[adapterPosition]
            binding.image.setImageResource(model.image)
            binding.name.text = model.name
            binding.weight.text = model.weight
        }
    }
}